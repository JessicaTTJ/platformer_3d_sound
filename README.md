# 3D Audio Platformer
This platformer has been created with Unity (version 2019.2.3f1). It makes use of 3D sound and the [Steam Audio](https://valvesoftware.github.io/steam-audio/) plug-in for additional 3D sound techniques. 

# Demo
A demo can be viewed here: https://www.youtube.com/watch?v=BXb6n50MFRE&feature=youtu.be 

![Ingame_screenshot](https://cdn.discordapp.com/attachments/617714009126207533/638861896597897229/in_game2.PNG)

The controls are: WASD to walk, SPACE to jump and left mouse-click to shoot.

# Requirements
- Unity Game Engine (preferably 2019 version since previous versions have not been tested)
- Basic knowledge about Unity and C#

# Download
You can clone the repository by copying: **git clone git@bitbucket.org:JessicaTTJ/platformer_3d_sound.git**

*Note: if you can't hear sound effects from the player (shooting, jumping, collecting pick-ups, knockback and colliding with objects), make sure 
to increase the 'max real voices' to 100 in your Unity settings (Edit > Project Settings > Audio).*

# Future improvements
These are some ideas to improve or built-upon the existing platformer which could be created by you!

- [FMOD](https://www.fmod.com/) integration for changing sounds depending on where the player is walking (e.g. on grass or on a bridge).
- Add more sound techniques, such as reflection (this comes with the Steam Audio plug-in).
- Extend the current audio handler so it handles all the sound effects in-game.

# Credits
**Free Unity assets:**

[5 animated Voxel animals](https://assetstore.unity.com/packages/3d/characters/animals/5-animated-voxel-animals-145754)

[Meshtint Free Tile Map Mega Toon Series](https://assetstore.unity.com/packages/3d/environments/meshtint-free-tile-map-mega-toon-series-153619) 

[Simple Gems Ultimate Animated Customizable Pack](https://assetstore.unity.com/packages/3d/props/simple-gems-ultimate-animated-customizable-pack-73764) 

[Voxel Environments 1](https://assetstore.unity.com/packages/3d/environments/fantasy/voxel-environments-1-152920)

**Background music**

Music from https://filmmusic.io/
"Magic Scout Farm" by Kevin MacLeod (https://incompetech.com/)
License: CC BY (http://creativecommons.org/licenses/by/4.0/)

Music from https://filmmusic.io
"Come Play with Me" by Kevin MacLeod (https://incompetech.com)
License: CC BY (http://creativecommons.org/licenses/by/4.0/)

Music from https://filmmusic.io
"Wholesome" by Kevin MacLeod (https://incompetech.com)
License: CC BY (http://creativecommons.org/licenses/by/4.0/)

[Freesound sound effects](https://freesound.org/)

**3D Platformer tutorial in Unity**

[gamesplusjames](https://www.youtube.com/watch?v=h2d9Wc3Hhi0&list=PLiyfvmtjWC_V_H-VMGGAZi7n5E0gyhc37)
