﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This class handles the behaviour of the visible enemy (that chases the player)
 * The enemyhealth.cs script could not be used because those are invisible enemies
 * 
 * */
public class VisibleEnemy : MonoBehaviour
{
    public GameObject enemyPrefab;
    AudioSource audioSource;
    public AudioClip penguinSound;
    public AudioClip bulletHitPenguinSound;
    public Transform playerTransform;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        audioSource.spatialBlend = 1.0f;
        audioSource.rolloffMode = AudioRolloffMode.Linear;
        audioSource.minDistance = 1;
        audioSource.maxDistance = 10;
        audioSource.PlayOneShot(bulletHitPenguinSound, 1f);
    }
}
