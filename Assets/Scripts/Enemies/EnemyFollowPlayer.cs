﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/*
 * This class handles the visible enemy behaviour to start chasing the player
 * 
 * */
public class EnemyFollowPlayer : MonoBehaviour
{
    public float lookRadius = 10f;
    Transform target;
    NavMeshAgent agent;

    void Start()
    {
        target = PlayerController.instance.playerModel.transform;
        agent = GetComponent<NavMeshAgent>();
    }

    void Update()
    {
        float distance = Vector3.Distance(target.position, transform.position); // calculates position between enemy and player

        // start chasing the player
        if(distance <= lookRadius)
        {
            agent.SetDestination(target.position);
        }
    }

    // Draws the look radius of the enemy
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }
}
