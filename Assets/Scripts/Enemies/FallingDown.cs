﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This class handles the falling ball behaviour
 * 
 * */
public class FallingDown : MonoBehaviour
{
    public Transform player; 
    public float maxDistance = 5f; //the distance between the ball and the player
    private Rigidbody rb;
    AudioSource audioSource;
    Renderer mr;
    public AudioClip warningSound;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.useGravity = false;
        audioSource = GetComponent<AudioSource>();
        mr = GetComponent<Renderer>();
    }

    void Update()
    {
        // calculates the distance between the ball and player before playing an sfx and making use of the rb's gravity
        if (Vector3.Distance(player.position, transform.position) < maxDistance)
        { 
            if (!audioSource.isPlaying)
            {
                audioSource.PlayOneShot(warningSound, 1.0f);
                audioSource.time = 1.0f;
            }
            rb.useGravity = true;
            Destroy(gameObject, 2f);
        }
    }
}
