﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This class handles the invisibility of enemies and the sound settings upon collision with the player
 * It has an unimplemented feature where the player can kill the enemy (as of now you can only make the enemy visible to avoid it)
 * because of level design decisions.
 * 
 * */
public class EnemyHealth : MonoBehaviour
{
    public GameObject enemyPrefab;
    public const int maxHealth = 1;
    public int health = maxHealth;

    public MeshRenderer[] enemyRenderer;
    AudioSource audioSource;
    public AudioClip penguinSound;
    public AudioClip bulletHitPenguinSound;

    public Transform playerTransform;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();

        // Makes the gameobject invisible but the interactivity remains
        foreach (Renderer r in enemyRenderer)
        {
            r.enabled = false;
        }        
    }

    public void TakeDamage(int amount)
    {
        health -= amount;
        if(health <= 0)
        {
            health = 0;
            Debug.Log("Pingu die");
        }
    }

    // Make the enemy visible for 5 seconds
    public IEnumerator Visible()
    {
        foreach (Renderer r in enemyRenderer)
        {
            r.enabled = true;
        }

        yield return new WaitForSeconds(5);
        foreach (Renderer r in enemyRenderer)
        {
            r.enabled = false;
        }
    }

    // Changes the audio settings from the Audio Source component
    private void OnCollisionEnter(Collision collision)
    {
        audioSource.spatialBlend = 1.0f;
        audioSource.rolloffMode = AudioRolloffMode.Linear;
        audioSource.minDistance = 1;
        audioSource.maxDistance = 10;
        audioSource.PlayOneShot(bulletHitPenguinSound, 1f);
        StartCoroutine("Visible");
    }

    // Kill the enemy
    //private void OnCollisionEnter(Collision collision)
    //{
    //    GameObject hit = collision.gameObject;
    //    EnemyHealth health = hit.GetComponent<EnemyHealth>();
    //    if (health != null)
    //    {
    //        health.TakeDamage(1);
    //    }
    //    Destroy(gameObject);
    //}
}
