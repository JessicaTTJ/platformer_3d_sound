﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This class handles the damage dealt to the player from enemies, water or moving objects
 * 
 * */
public class HurtPlayer : MonoBehaviour
{
    public int damageToGive = 1;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            // player gets hit, checks what direction it gets hit from
            Vector3 hitDirection = other.transform.position - transform.position;
            hitDirection = hitDirection.normalized;

            FindObjectOfType<HealthManager>().HurtPlayer(damageToGive, hitDirection);
        }
    }
}
