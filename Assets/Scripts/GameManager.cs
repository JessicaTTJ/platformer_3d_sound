﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * This class handles the pick-up (stars) and UI text
 * 
 * */
public class GameManager : MonoBehaviour
{
    public int currentAmount;
    public Text pickupText;

    public void AddPickup(int pickupToAdd)
    {
        currentAmount += pickupToAdd;
        pickupText.text = "Stars: " + currentAmount;
    }
}
