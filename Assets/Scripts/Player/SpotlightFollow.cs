﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This class handles the spotlight following the player.
 * The spotlight has been assigned as a child to the player so it is always above the player. 
 * If it's not a child, the spotlight could depend on the angle of the camera.
 * 
 * */
public class SpotlightFollow : MonoBehaviour
{
    public GameObject player;

    void LateUpdate()
    {
        transform.LookAt(player.transform);
    }
}
