﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * This class handles the player behaviour when getting hit by enemies (health, invincibility and respawning)
 * 
 * */
public class HealthManager : MonoBehaviour
{
    // health
    public int maxHealth;
    public int currentHealth;

    // for knockback
    public PlayerController player;

    // for invincibility (so the player doesn't get damaged twice when bouncing off another enemy/object)
    public float invincibilityLength; // how long player stays invincible for
    private float invincibilityCounter; // how long he stays in invincibility-mode

    // Give feedback to player that he has been damaged (flickering player model)
    public Renderer [] playerRenderer;
    private float flashCounter;
    public float flashLength = 0.1f;

    // for respawning the player
    private bool isRespawning;
    private Vector3 respawnPoint;
    public float respawnLength;

    // Heal text
    public Text healthText;

    // Lives left sound
    AudioSource audioSource;
    public AudioClip twoLives;
    public AudioClip oneLive;
    public AudioClip gameOver;

    void Start()
    {
        currentHealth = maxHealth;

        // Need to enable/disable Character Controller so the player will respawn 
        // where he spawned at the start of the scene (instead of where he died)
        GameObject player = GameObject.Find("Player");       
        CharacterController charController = player.GetComponent<CharacterController>();
        charController.enabled = false;
        respawnPoint = player.transform.position;
        charController.enabled = true;

        audioSource = GetComponent<AudioSource>();

    }

    void Update()
    {
        // Find instance of GameManager in the game scene, then add pickup value
        healthText.text = "Health: " + currentHealth;

        // check how long the player is invincible for when recently getting hurt by objects
        // that is how long the flash counter will last, otherwise the player returns back to normal
        if (invincibilityCounter > 0)
        {
            invincibilityCounter -= Time.deltaTime;

            flashCounter -= Time.deltaTime;
            // flash the player character 
            if(flashCounter <= 0)
            {
                foreach (Renderer r in playerRenderer)
                {
                    r.enabled = !r.enabled;
                    flashCounter = flashLength;
                }
            }
            
            if(invincibilityCounter <= 0)
            {
                foreach (Renderer r in playerRenderer)
                {
                    r.enabled = true;
                }
            }
        }
    }
    
    public void HurtPlayer(int damage, Vector3 direction) // handles damage, health left and respawn
    {     
        if (invincibilityCounter <= 0)
        {
            currentHealth -= damage;

            if (currentHealth <= 0)
            {              
                Respawn();
            }
            else
            {
                player.Knockback(direction);

                invincibilityCounter = invincibilityLength;

                foreach (Renderer r in playerRenderer)
                {
                    r.enabled = false;
                    flashCounter = flashLength;
                }
            }
        }
        if(currentHealth == 2)
        {
            audioSource.PlayOneShot(twoLives, 1f);
        }
        if (currentHealth == 1)
        {
            audioSource.PlayOneShot(oneLive, 1f);
        }
        if (currentHealth <= 0)
        {
            audioSource.PlayOneShot(gameOver, 1f);
        }
    }

    public void Respawn()
    {
        if (!isRespawning)
        {
            audioSource.PlayOneShot(gameOver, 1f);

            StartCoroutine("RespawnCo");
        }
    }

    public IEnumerator RespawnCo()
    {
        isRespawning = true;
        player.gameObject.SetActive(false);
        yield return new WaitForSeconds(respawnLength);
        isRespawning = false;
        player.gameObject.SetActive(true);
        player.transform.position = respawnPoint;
        currentHealth = maxHealth;

        invincibilityCounter = invincibilityLength;

        foreach (Renderer r in playerRenderer)
        {
            r.enabled = false;
            flashCounter = flashLength;
        }
    }

    public void HealPlayer(int healAmount)
    {
        currentHealth += healAmount;

        // Prevents overhealing
        if(currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }
    }

    public void SetSpawnPoint(Vector3 newPosition) // for checkpoints
    {
        respawnPoint = newPosition; 
    }
}
