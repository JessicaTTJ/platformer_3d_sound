﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This class handles the player controller (moving, jumping, shooting and knockback force)
 * 
 * */
public class PlayerController : MonoBehaviour
{
    public float moveSpeed;
    public float jumpForce;

    public CharacterController controller;

    private Vector3 moveDirection;
    public float gravityScale;

    // Animation related
    public Animator anim;

    // rotation related
    public Transform pivot;
    public float rotateSpeed;
    public GameObject playerModel;

    // knockback related
    public float knockbackForce;
    public float knockbackTime;
    private float knockbackCounter; // whether being knockbacked at the moment and how long the player is knockbacked for

    // For bullet/shooting objects to make them visible later on
    public GameObject bulletPrefab;
    public Transform bulletSpawn;

    // Audio related
    AudioSource audioSource;
    public AudioClip jumpingSound;
    public AudioClip knockbackPlayerSound;
    public AudioClip shootEggSound;

    // For enemies to chase the player without having to use find.gameObject
    #region Singleton
    public static PlayerController instance;

    void Awake()
    {
        instance = this;
    }


    #endregion

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        controller = GetComponent<CharacterController>();
    }

    void Update()
    {
        // Shoot eggs/bullets on left-mouse click
        if (Input.GetMouseButtonDown(0))
        {
            Fire();
        }

        if (knockbackCounter <= 0) // if currently not in knockback-mode
        {
            float yStore = moveDirection.y;
            moveDirection = (transform.forward * Input.GetAxis("Vertical")) + (transform.right * Input.GetAxis("Horizontal"));
            moveDirection = moveDirection.normalized * moveSpeed;
            moveDirection.y = yStore;

            if (controller.isGrounded)
            {
                moveDirection.y = 0f; // handles gravity, jumps down from a high platform smoothly
                if (Input.GetButtonDown("Jump"))
                {
                    moveDirection.y = jumpForce;
                    audioSource.PlayOneShot(jumpingSound, 1f);
                }
            }
        }
        else
        {
            knockbackCounter -= Time.deltaTime;
            if (!audioSource.isPlaying)
            {
                audioSource.PlayOneShot(knockbackPlayerSound, 0.1f);
            }
        }
        moveDirection.y = moveDirection.y + (Physics.gravity.y * gravityScale * Time.deltaTime);
        controller.Move(moveDirection * Time.deltaTime);

        // move player in different direction based on pivot point/camera direction
        if(Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
        {
            // rotate player
            transform.rotation = Quaternion.Euler(0f, pivot.rotation.eulerAngles.y, 0f);

            // player faces different direction
            Quaternion newRotation = Quaternion.LookRotation(new Vector3(moveDirection.x, 0f, moveDirection.z));
            playerModel.transform.rotation = Quaternion.Slerp(playerModel.transform.rotation, newRotation, rotateSpeed * Time.deltaTime);

        }

        anim.SetBool("isGrounded", controller.isGrounded);
        anim.SetFloat("Speed", (Mathf.Abs(Input.GetAxis("Vertical"))+ Mathf.Abs(Input.GetAxis("Horizontal"))));
    }

    public void Knockback(Vector3 direction)
    {
        knockbackCounter = knockbackTime;

        moveDirection = direction * knockbackForce;

        // get knocked up in the air
        moveDirection.y = knockbackForce;
    }

    private void Fire()
    {
        // create bullet/egg from prefab
        GameObject bullet = Instantiate(bulletPrefab, bulletSpawn.position, bulletSpawn.rotation);

        float shootSpeed = 6.0f;
        bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * shootSpeed;
        audioSource.PlayOneShot(shootEggSound, 1f);

        // destroy bullet after 2 seconds
        Destroy(bullet, 2);
    }

}
