﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This class handles the moving platform so the player stops moving when the platform moves.
 * 
 * */
public class MovingPlatform : MonoBehaviour
{
    public GameObject player;

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject == player)
        {
            player.transform.parent = transform;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject == player)
        {
            player.transform.parent = null;
        }
    }
}
