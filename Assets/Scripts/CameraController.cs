﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This class handles the camera movement with the mouse and for following the player
 * 
 * */
public class CameraController : MonoBehaviour
{
    public Transform target;
    public Vector3 offset;

    // for manual offset values inside the inspector
    // changes the cameras view of the target
    public bool useOffsetValues;

    public float rotateSpeed; // rotating camera

    public Transform pivot;

    // For looking up/down with the camera
    public float maxViewAngle;
    public float minViewAngle;
    private float maxRotation = 360f;
    private float halfRotation = 180f;

    // Invert Y-axis camera (if your cursor moves up, the camera moves up and vice versa)
    public bool invertY;

    void Start()
    {
        if (!useOffsetValues)
        {
            // get distance of camera and player
            offset = target.position - transform.position;
        }

        // for moving up and down (player)
        pivot.transform.position = target.transform.position;
        pivot.transform.parent = null;

        // hide cursor upon start of game but for now, it is needed to click on the sound settings (UI)
       // Cursor.lockState = CursorLockMode.Locked;
    }

    void LateUpdate()
    {
        // related to animating the player (rotation)
        pivot.transform.position = target.transform.position;

        // get x pos of mouse & rotate target
        float horizontal = Input.GetAxis("Mouse X") * rotateSpeed;
        pivot.Rotate(0, horizontal, 0);

        // get y pos of mouse & rotate pivot
        float vertical = Input.GetAxis("Mouse Y") * rotateSpeed;

        if (invertY)
        {
            pivot.Rotate(vertical, 0, 0);
        }
        else
        {
            pivot.Rotate(-vertical, 0, 0);
        }

        // Limit up/down camera rotation
        if(pivot.rotation.eulerAngles.x > maxViewAngle && pivot.rotation.eulerAngles.x < halfRotation)
        {
            pivot.rotation = Quaternion.Euler(maxViewAngle, 0, 0);
        }

        if (pivot.rotation.eulerAngles.x > halfRotation && pivot.rotation.eulerAngles.x < maxRotation + minViewAngle)
        {
            pivot.rotation = Quaternion.Euler(maxRotation + minViewAngle, 0, 0);
        }


        // move camera based on current rotation of target & original offset
        float desiredYAngle = pivot.eulerAngles.y;
        float desiredXAngle = pivot.eulerAngles.x;
        Quaternion rotation = Quaternion.Euler(desiredXAngle, desiredYAngle, 0);
        transform.position = target.position - (rotation * offset);

        // if camera goes below the height of player,
        if(transform.position.y < target.position.y)
        {
            transform.position = new Vector3(transform.position.x, target.position.y -.5f, transform.position.z);
        }

        // camera looks at the target/player
        transform.LookAt(target);
    }
}
