﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This class handles the audio within the game so many (real-time) sounds in the game world do not need to be checked every second.
 * It could be expanded by changing all scripts that use audio source components to this audio handler script.
 * This way, you don't need to change the 'real voices' limit in the Unity Audio project settings.
 * 
 * */
public class AudioHandler : MonoBehaviour
{
    bool canPlay;

    public void PlayAudio(AudioClip clip)
    {
        if (canPlay)
        {
            canPlay = false;
        }
        GetComponent<AudioSource>().PlayOneShot(clip);

        StartCoroutine(Reset());
    }

    IEnumerator Reset()
    {
        yield return new WaitForSeconds(.2f);
        canPlay = true;
    }
}
