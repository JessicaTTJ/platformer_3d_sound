﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This class handles the pickup on collision with the player
 * 
 * */
public class PickUp : MonoBehaviour
{
    AudioSource audioSource;
    public int value;
    public AudioClip obtainedSound;
    public bool isAlreadyPlaying = false;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    // Check if player collides with pick up
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player")
        {
            if (!isAlreadyPlaying)
            {
                audioSource.PlayOneShot(obtainedSound, 0.5f);

                isAlreadyPlaying = true;
            }
            // Find instance of GameManager in the game scene, then add pickup value
            FindObjectOfType<GameManager>().AddPickup(value);

            Destroy(gameObject,0.2f);
        }
    }
}
