﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

/*
 * This class handles the sound settings UI by making them (dis)appear on the screen
 * and using the values from Unity's audio mixer to change the volume. 
 * 
 * */
public class SoundOptionMenu : MonoBehaviour
{
    private Button soundSettingsButton;
    public Image image;
    public Text musicTxt;
    public Text fxTxt;
    public Slider musicSlider;
    public Slider fxSlider;
    public Button closeButton;

    public AudioMixer audioMixer;
    public AudioMixer sfxMixer;

    void Start()
    {
        image.enabled = false;
        musicTxt.enabled = false;
        fxTxt.enabled = false;
        musicSlider.gameObject.SetActive(false);
        fxSlider.gameObject.SetActive(false);
        closeButton.gameObject.SetActive(false);

    }

    public void OpenSoundSettings()
    {
        image.enabled = true;
        musicTxt.enabled = true;
        fxTxt.enabled = true;
        musicSlider.gameObject.SetActive(true);
        fxSlider.gameObject.SetActive(true);
        closeButton.gameObject.SetActive(true);

    }

    public void CloseSoundSettings()
    {
        image.enabled = false;
        musicTxt.enabled = false;
        fxTxt.enabled = false;
        musicSlider.gameObject.SetActive(false);
        fxSlider.gameObject.SetActive(false);
        closeButton.gameObject.SetActive(false);

    }

    // Unity's audio mixer 
    public void SetVolume(float volume)
    {
        //  Debug.Log(volume);
        audioMixer.SetFloat("volume", volume);
    }
    // Unity's audio mixer
    public void SetfxVolume(float volume)
    {
        //  Debug.Log(volume);
        sfxMixer.SetFloat("sfxVolume", volume);
    }

}
