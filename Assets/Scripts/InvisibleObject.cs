﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This class makes a gameobject invisible by disabling the mesh renderer
 * This is not used in the game anymore but is for testing purposes (with invisible enemies)
 * See: 'deprecated GameObjects' 
 * */
public class InvisibleObject : MonoBehaviour
{
    MeshRenderer mr;

    void Start()
    {
        mr = GetComponent<MeshRenderer>();
        // Makes the gameobject invisible but the interactivity remains
        mr.enabled = false;
    }

}
