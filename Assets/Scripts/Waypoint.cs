﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This class handles the collision between a waypoint (except for the last waypoint) and the player. 
 * It also handles the checkpoints.
 * 
 * */
public class Waypoint : MonoBehaviour
{
    public HealthManager healthManager; // for checkpoints because respawning is handled in the healthmanager
    AudioSource audioSource;
    public AudioClip victorySound;
    public bool isAlreadyPlaying = false;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        healthManager = FindObjectOfType<HealthManager>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (!isAlreadyPlaying)
            {
                audioSource.PlayOneShot(victorySound, 0.5f);
                isAlreadyPlaying = true;
                healthManager.SetSpawnPoint(transform.position);
            }
        }
    }
}
