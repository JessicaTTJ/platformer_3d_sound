﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
 * This class handles the collision between the last waypoint and the player
 * to display UI text and play a different victory sound.
 * 
 * */
public class Waypoint_endgame : MonoBehaviour
{
    AudioSource audioSource;
    public AudioClip victorySound;
    public bool isAlreadyPlaying = false;
    public Text endGame;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        endGame.enabled = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (!isAlreadyPlaying)
            {
                audioSource.PlayOneShot(victorySound, 0.5f);
                isAlreadyPlaying = true;
                endGame.enabled = true;
                endGame.text = "Congratulations! You finished the game!";
                StartCoroutine(RemoveText());
            }
        }
    }

    IEnumerator RemoveText()
    {
        yield return new WaitForSeconds(5f);
        endGame.enabled = false;
    }
}
