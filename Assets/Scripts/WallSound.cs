﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This class handles the collision of the player's bullet and a wall to start a sfx
 * 
 * */
public class WallSound : MonoBehaviour
{
    AudioHandler aHandler;
    public AudioClip bulletHitWallSound;

    void Start()
    {
        aHandler = GameObject.Find("Audio_Handler_FX").GetComponent<AudioHandler>();
    }

    private void OnTriggerEnter(Collider other)
    {
        aHandler.PlayAudio(bulletHitWallSound);
    }
}
