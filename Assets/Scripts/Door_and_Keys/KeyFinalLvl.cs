﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This class handles the collision between the final 2 keys and player 
 * 
 * */
public class KeyFinalLvl : MonoBehaviour
{
    public static int keyAmount;
    AudioSource audioSource;
    public AudioClip obtainedSound;
    public bool isAlreadyPlaying = false;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            keyAmount++;
            Debug.Log("Key amount lvl 3 is: " + keyAmount);
            if (!isAlreadyPlaying)
            {
                audioSource.PlayOneShot(obtainedSound, 0.5f);
                isAlreadyPlaying = true;
                Destroy(gameObject, 0.2f);
            }
        }
    }
}
