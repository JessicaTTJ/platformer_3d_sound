﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This class handles the collision between the first key and player 
 * 
 * */
public class Key : MonoBehaviour
{
    AudioSource audioSource;
    public AudioClip obtainedSound;
    public bool isAlreadyPlaying = false;
    public static bool playerHasKey = false;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            if (!isAlreadyPlaying)
            {
                audioSource.PlayOneShot(obtainedSound, 0.5f);

                isAlreadyPlaying = true;
            }
            playerHasKey = true;
            if (playerHasKey)
            {
                 Destroy(gameObject,0.2f);
                //For testing sound purposes e.g duration: gameObject.SetActive(false);
            }
        }
    }
}
