﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This class handles the collision between the final door and player 
 * 
 * */
public class DoorFinalLvl : MonoBehaviour
{

    AudioHandler aHandler;
    public AudioClip doorOpenSound;
    public AudioClip bulletHitDoorSound;
    public bool isAlreadyPlaying = false; // sfx

    void Start()
    {
        aHandler = GameObject.Find("Audio_Handler_FX").GetComponent<AudioHandler>();
    }

    // Check if player collides with pick up
    private void OnTriggerEnter(Collider other)
    {
        aHandler.PlayAudio(bulletHitDoorSound);

        if (other.tag == "Player")
        {
            if (!isAlreadyPlaying && KeyFinalLvl.keyAmount > 3) // the player only needs 2 keys but there's a strange bug (with animations) that counts 1 key as 1-3 keys..
            {
                aHandler.PlayAudio(doorOpenSound);
                isAlreadyPlaying = true;

                Debug.Log("Player has opened the 3rd door!");
                Destroy(gameObject, 4f);
            }
        }
    }
}
