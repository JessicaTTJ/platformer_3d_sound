﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This class handles the collision between the first door and player 
 * 
 * */
public class Door : MonoBehaviour
{
    AudioHandler aHandler;
    public AudioClip doorOpenSound;
    public AudioClip bulletHitDoorSound;
    public bool isAlreadyPlaying = false;

    void Start()
    {
        aHandler = GameObject.Find("Audio_Handler_FX").GetComponent<AudioHandler>();
    }

    // Check if player collides with pick up
    private void OnTriggerEnter(Collider other)
    {
        aHandler.PlayAudio(bulletHitDoorSound);

        if (other.tag == "Player")
        {
            // if the sfx is not already playing and the player has picked up the key
            if (!isAlreadyPlaying && Key.playerHasKey)
            {
                aHandler.PlayAudio(doorOpenSound);
                isAlreadyPlaying = true;

                Debug.Log("Player has opened the door!");
                Destroy(gameObject, 4f);
            }

        }
    }
}
