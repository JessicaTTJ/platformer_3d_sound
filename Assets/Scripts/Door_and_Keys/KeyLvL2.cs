﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * This class handles the collision between the second level keys and player 
 * 
 * */
public class KeyLvL2 : MonoBehaviour
{
    public static int keyAmount;
    AudioSource audioSource;
    public AudioClip obtainedSound;
    public bool isAlreadyPlaying = false;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            keyAmount++;
            Debug.Log("Key amount is: " + keyAmount);
            if (!isAlreadyPlaying)
            {
                audioSource.PlayOneShot(obtainedSound, 0.5f);
                isAlreadyPlaying = true;
                Destroy(gameObject, 0.2f);
            }
        }
    }
}
